from sanic_openapi import swagger_blueprint

from fwork.common.env.version import get_app_version
from fwork.common.sanic.app import make_app, run_app
from source.application.endpoints.accounts import Accounts
from source.application.endpoints.auth import AuthView
from source.application.endpoints.company import CompanyInitView, CompanyView
from source.application.endpoints.registry import BusinessTypeView, OrganizationTypeView, PermissionsView, RolesView
from source.application.endpoints.token_refresh import RefreshTokenView
from source.application.utils.template import load_templates
from source.constants import PROJECT_NAME
from source.settings import EMPLOYEE_TMPL, TEMPLATES_DIR

app = make_app(PROJECT_NAME, use_postgres=True, use_sentry=False, use_smtp=True)

app.config.API_DESCRIPTION = f"{app.name} service API, app version: {get_app_version()}"

V1 = '/api/v1'
# Register views to handle requests.
app.add_route(Accounts.as_view(), f'{V1}/accounts')
app.add_route(AuthView.as_view(), f'{V1}/auth')
app.add_route(RefreshTokenView.as_view(), f'{V1}/token/refresh')
app.add_route(RolesView.as_view(), f'{V1}/roles')
app.add_route(CompanyView.as_view(), f'{V1}/companies')
app.add_route(CompanyInitView.as_view(), f'{V1}/accounts/<user_id:int>/companies')
app.add_route(PermissionsView.as_view(), f'{V1}/permissions')
app.add_route(BusinessTypeView.as_view(), f'{V1}/business_types')
app.add_route(OrganizationTypeView.as_view(), f'{V1}/organization_types')

# Enable OpenApi spec generation
app.blueprint(swagger_blueprint)

# Should help to use templates only once per instance
app.employee_template = load_templates(TEMPLATES_DIR, EMPLOYEE_TMPL)

if __name__ == "__main__":
    run_app(app)
