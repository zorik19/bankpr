from fwork.common.env import env

AUTH_TOKEN_SECRET = env.str('AUTH_TOKEN_SECRET', default='secret')
REFRESH_TOKEN_SECRET = env.str('REFRESH_TOKEN_SECRET', default='secret_refresh')

RETURN_TOKENS_ON_LINK_PASSWORD_CHANGE = env.int('RETURN_TOKENS_ON_LINK_PASSWORD_CHANGE', default=1)

# JWT expiration time in seconds
AUTH_TOKEN_EXP_TIME = env.int('AUTH_TOKEN_EXP_TIME', default=60 * 60 * 3)
REFRESH_TOKEN_EXP_TIME = env.int('REFRESH_TOKEN_EXP_TIME', default=60 * 60 * 24 * 7)

TEMPLATES_DIR = env.str('TEMPLATES_DIR', default='application/templates')
EMPLOYEE_TMPL = env.str('BODY_TMPL', default='employee.html')
