import datetime

from marshmallow import fields, post_load, Schema, validates_schema, ValidationError


class ProfileSchema(Schema):
    first_name = fields.Str(allow_none=True)
    second_name = fields.Str(allow_none=True)
    last_name = fields.Str(allow_none=True)
    gender = fields.Str(allow_none=True)
    birthdate = fields.Str(allow_none=True)
    avatar = fields.Str(allow_none=True)

    @post_load
    def remove_nones(self, data, **kwargs):
        return {k: v for k, v in data.items() if v is not None}

    @validates_schema
    def validate_datetime(self, data: dict, **kwargs):
        date_format = "%d-%m-%Y"
        if data.get("birthdate"):
            try:
                datetime.datetime.strptime(data["birthdate"], date_format)
            except ValueError:
                raise ValidationError(f"Valid date should be {date_format}")
