from marshmallow import Schema
from marshmallow_jsonapi import fields, Schema as JsonApiScheme

# TODO: on the fly generation of sxhemes based on model


class RegistryBaseResponseSchema(Schema):
    """Base registry """
    id = fields.Int()
    name = fields.Str()
    description = fields.Str()


class RoleResponseSchema(RegistryBaseResponseSchema, JsonApiScheme):
    is_active = fields.Boolean()

    class Meta:
        type_ = 'role'


class PermissionResponseSchema(RegistryBaseResponseSchema, JsonApiScheme):
    class Meta:
        type_ = 'permission'


class OrganizationResponseSchema(RegistryBaseResponseSchema, JsonApiScheme):
    class Meta:
        type_ = 'organization'


class BusinessTypeResponseSchema(RegistryBaseResponseSchema, JsonApiScheme):
    okved = description = fields.Str()

    class Meta:
        type_ = 'business_type'
        exclude = ("description",)
