from marshmallow_jsonapi import fields, Schema


class TokenSchema(Schema):
    id = fields.Int(allow_none=True)
    token = fields.Str()
    refresh_token = fields.Str(allow_none=True)

    class Meta:
        type_ = 'token'
