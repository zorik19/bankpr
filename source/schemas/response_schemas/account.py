from marshmallow_jsonapi import fields, Schema

from source.schemas.company import CompanySchema


class AccountResponseSchema(Schema):
    id = fields.Int()
    email = fields.Email()
    phone_number = fields.Str()
    profile = fields.Dict()
    is_active = fields.Boolean()
    is_email_confirmed = fields.Boolean()

    class Meta:
        type_ = 'account'


class AccountResponseGetSchema(AccountResponseSchema):
    company = fields.List(fields.Nested(CompanySchema))

    class Meta:
        type_ = 'account'
