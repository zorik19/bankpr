from marshmallow_jsonapi import fields, Schema

from source.schemas.company import CompanySchema


class CompanyResponseSchema(CompanySchema, Schema):
    id = fields.Str()

    class Meta:
        type_ = 'company'
