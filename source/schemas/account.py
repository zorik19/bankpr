from marshmallow import fields, Schema, validates_schema, ValidationError

from source.schemas.password import PasswordSchema
from source.schemas.profile import ProfileSchema


class CredentialsSchema(PasswordSchema):
    email = fields.Email(allow_none=False, required=True,
                         description='Either valid email or phone number must be provided.')
    phone_number = fields.Str(allow_none=True, description='Either valid email or phone number must be provided.')

    @validates_schema
    def validate_identifier(self, data: dict, **kwargs):
        if not (data.get('email') or data.get('phone_number')):
            raise ValidationError('Valid email or phone number must be provided.')


class AccountRegisterSchema(CredentialsSchema):
    department_id = fields.Int()
    role_id = fields.Int(required=True)
    profile = fields.Nested(ProfileSchema)


class AccountEditSchema(Schema):
    email = fields.Email()
    phone_number = fields.Str()
    profile = fields.Nested(ProfileSchema)


class AccountFilterSchema(Schema):
    company = fields.Bool(missing=False, description='Get account with company info?')


class TokenRefreshSchema(Schema):
    refresh_token = fields.Str(required=True)