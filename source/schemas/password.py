from marshmallow import fields, Schema, ValidationError

from source.constants import PASSWORD_MAX_LENGTH, PASSWORD_MIN_LENGTH


def validate_password(value):
    """
    validate common password conditions

    :param value: str password
    :return: ValidationError if fail
    """
    if len(value) < PASSWORD_MIN_LENGTH:
        raise ValidationError(f'Password must be at least {PASSWORD_MIN_LENGTH} characters long')
    elif len(value) > PASSWORD_MAX_LENGTH:
        raise ValidationError(f'Password must be no longer than {PASSWORD_MAX_LENGTH} characters')


class PasswordSchema(Schema):
    password = fields.Str(required=True, validate=validate_password, description='At least 6 characters longs')


class ChangePasswordSchema(Schema):
    old_password = fields.Str(required=True)
    new_password = fields.Str(required=True, validate=validate_password)
