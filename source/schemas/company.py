from marshmallow import fields, Schema

from source.schemas.department import DepartmentWithAccountSchema


class CompanySchema(Schema):
    name = fields.Str(required=True, descritption='Name of company')
    business_type_id = fields.Int(required=True, descritption='Business type ID')
    organization_id = fields.Int(required=True, descritption='OrganizationType type')
    site = fields.Str(allow_none=True, descritption='Site of company')
    address = fields.Str(allow_none=True, descritption='Legal address of company')


class CompanyInitSchema(CompanySchema):
    departments = fields.List(fields.Nested(DepartmentWithAccountSchema, exclude=('company_id',)))
