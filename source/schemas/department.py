from marshmallow import fields, Schema

from source.schemas.account import AccountRegisterSchema


class DepartmentSchema(Schema):
    name = fields.Str(required=True, description='Department name')
    company_id = fields.Str(required=True, description='Related company ID')
    description = fields.Str(required=False, description='Description metainfo about department')


class DepartmentWithAccountSchema(DepartmentSchema):
    accounts = fields.List(fields.Nested(AccountRegisterSchema, exclude=('password', 'department_id',)))
