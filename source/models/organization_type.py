from sqlalchemy import BigInteger, Column, DateTime, func, VARCHAR

from fwork.common.db.postgres.conn_async import db


class OrganizationType(db.Model):
    __tablename__ = 'organization_types'

    id = Column(BigInteger, primary_key=True)
    name = Column(VARCHAR(length=200), nullable=False, comment='Краткое название типа организации')
    description = Column(VARCHAR(length=1000), nullable=True, comment='Краткое описание')
    created_at = Column(DateTime(timezone=True), server_default=func.current_timestamp())
    modified_at = Column(DateTime(timezone=True), onupdate=func.current_timestamp())

    def __repr__(self):
        return f'<OrganizationType(' \
               f'id={self.id}, ' \
               f'name={self.name}, ' \
               f'description={self.description}, ' \
               f'created_at={self.created_at}, ' \
               f'modified_at={self.modified_at}' \
               f')>'
