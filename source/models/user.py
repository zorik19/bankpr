from sqlalchemy import BigInteger, Boolean, Column, DateTime, ForeignKey, func
from sqlalchemy.dialects.postgresql import JSONB, VARCHAR

from fwork.common.db.postgres.conn_async import db


class User(db.Model):
    __tablename__ = 'users'

    id = Column(BigInteger, primary_key=True)
    department_id = Column(BigInteger, ForeignKey('departments.id'), nullable=True, comment='id направления')
    role_id = Column(BigInteger, ForeignKey("roles.id"), comment='Роль')
    email = Column(VARCHAR(length=200), index=True, comment='email')
    phone_number = Column(VARCHAR(length=20), index=True, comment='Телефон')
    profile = Column(JSONB, comment='Профиль пользователя')
    is_banned = Column(Boolean, server_default="0", comment="Флаг забаненного пользователя")
    is_deleted = Column(Boolean, server_default="0", comment="Флаг удаленного пользователя")
    is_email_confirmed = Column(Boolean, server_default="0", comment="Флаг подтвержденного email")
    password = Column(VARCHAR(length=200))
    created_at = Column(DateTime(timezone=True), server_default=func.current_timestamp())
    modified_at = Column(DateTime(timezone=True), onupdate=func.current_timestamp())

    def __repr__(self):
        return f'<User(' \
               f'id={self.id}, ' \
               f'email={self.email}, ' \
               f'phone_number={self.phone_number}, ' \
               f'department_id={self.department_id}, ' \
               f'profile={self.profile}, ' \
               f'is_banned={self.is_banned},' \
               f'is_deleted={self.is_deleted},' \
               f'is_email_confirmed={self.is_email_confirmed}, ' \
               f'created_at={self.created_at}, ' \
               f'modified_at={self.modified_at}' \
               f')>'
