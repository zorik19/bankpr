from sqlalchemy import BigInteger, Column, DateTime, ForeignKey, func, VARCHAR

from fwork.common.db.postgres.conn_async import db


class Department(db.Model):
    __tablename__ = 'departments'

    id = Column(BigInteger, primary_key=True)
    name = Column(VARCHAR(length=200), nullable=False, comment='Название направления (департамента)')
    company_id = Column(BigInteger, ForeignKey('companies.id'), comment='id связанной компании')
    description = Column(VARCHAR(length=1000), nullable=True, comment='Краткое описание')
    created_at = Column(DateTime(timezone=True), server_default=func.current_timestamp())
    modified_at = Column(DateTime(timezone=True), onupdate=func.current_timestamp())

    def __repr__(self):
        return f'<Department(' \
               f'id={self.id}, ' \
               f'name={self.name}, ' \
               f'company_id={self.company_id}, ' \
               f'description={self.description}, ' \
               f')>'
