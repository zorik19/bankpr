from sqlalchemy import BigInteger, Column, DateTime, func, VARCHAR

from fwork.common.db.postgres.conn_async import db


class BusinessType(db.Model):
    __tablename__ = 'business_types'

    id = Column(BigInteger, primary_key=True)
    name = Column(VARCHAR(length=200), nullable=False, comment='Наименование раздела / группы ОКВЭД')
    okved = Column(VARCHAR(length=500), nullable=True, comment='Код_Раздела или Группы')
    created_at = Column(DateTime(timezone=True), server_default=func.current_timestamp())
    modified_at = Column(DateTime(timezone=True), onupdate=func.current_timestamp())

    def __repr__(self):
        return f'<BusinessType(' \
               f'id={self.id}, ' \
               f'name={self.name}, ' \
               f'okved={self.okved}, ' \
               f'created_at={self.created_at}, ' \
               f'modified_at={self.modified_at}' \
               f')>'
