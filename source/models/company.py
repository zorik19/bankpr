from sqlalchemy import BigInteger, Column, DateTime, ForeignKey, func, VARCHAR

from fwork.common.db.postgres.conn_async import db


class Company(db.Model):
    __tablename__ = 'companies'

    id = Column(BigInteger, primary_key=True)
    name = Column(VARCHAR(length=200), nullable=False, comment='Название компании')
    # FIXME: business types could be many
    business_type_id = Column(BigInteger, ForeignKey('business_types.id'), index=True, comment='Направление бизнеса')
    organization_id = Column(BigInteger, ForeignKey('organization_types.id'), index=True, comment='Тип организации')
    site = Column(VARCHAR(length=200), nullable=True, comment='Сайт компании')
    address = Column(VARCHAR(length=1000), nullable=True, comment='Юридический адресс')
    created_at = Column(DateTime(timezone=True), server_default=func.current_timestamp())
    modified_at = Column(DateTime(timezone=True), onupdate=func.current_timestamp())

    def __repr__(self):
        return f'<Company(' \
               f'id={self.id}, ' \
               f'name={self.name}, ' \
               f'site={self.site}, ' \
               f'address={self.address}, ' \
               f'business_type_id={self.business_type_id},' \
               f')>'
