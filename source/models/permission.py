from sqlalchemy import BigInteger, Column, DateTime, func, VARCHAR, ForeignKey, PrimaryKeyConstraint

from fwork.common.db.postgres.conn_async import db


class Permission(db.Model):
    __tablename__ = 'permissions'

    id = Column(BigInteger, primary_key=True)
    name = Column(VARCHAR(length=200), nullable=False, comment='Название разрешения')
    description = Column(VARCHAR(length=1000), nullable=True, comment='Краткое описание')
    created_at = Column(DateTime(timezone=True), server_default=func.current_timestamp())
    modified_at = Column(DateTime(timezone=True), onupdate=func.current_timestamp())

    def __repr__(self):
        return f'<Permission(' \
               f'id={self.id}, ' \
               f'name={self.name}, ' \
               f'description={self.description},' \
               f')>'


# Many2Many relations to link roles and permissions
class PermissionToRole(db.Model):
    __tablename__ = 'permissions_to_roles'

    role_id = Column(BigInteger, ForeignKey("roles.id"))
    permission_id = Column(BigInteger, ForeignKey("permissions.id"))

    __table_args__ = (
        PrimaryKeyConstraint('role_id', 'permission_id', name='permissions_to_roles_pk'), {}
        )

    def __repr__(self):
        return f'<PermissionToRole(' \
               f'role_id={self.role_id}, ' \
               f'permission_id={self.permission_id} ' \
               f')>'
