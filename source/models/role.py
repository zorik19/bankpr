from sqlalchemy import BigInteger, Boolean, Column, VARCHAR, DateTime, func

from fwork.common.db.postgres.conn_async import db


class Role(db.Model):
    __tablename__ = 'roles'

    id = Column(BigInteger, primary_key=True)
    name = Column(VARCHAR(length=200), nullable=False, comment='Наименование роли')
    description = Column(VARCHAR(length=1000), nullable=True, comment="Описание")
    is_active = Column(Boolean, server_default="1")
    created_at = Column(DateTime(timezone=True), server_default=func.current_timestamp())
    modified_at = Column(DateTime(timezone=True), onupdate=func.current_timestamp())

    def __repr__(self):
        return f'<Role(' \
               f'id={self.id}, ' \
               f'name={self.name}, ' \
               f'description={self.description},' \
               f'is_active={self.is_active},' \
               f')>'
