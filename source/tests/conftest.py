import pytest

from fwork.common.db.postgres.conn_async import db
from fwork.common.db.postgres.settings import DSN
from source.app import app
from source.application.utils.token import make_access_and_refresh_tokens
from source.models.user import User


@pytest.yield_fixture
def test_app():
    """
    Application test fixture.

    :return: Sanic app
    """
    yield app


@pytest.fixture
def test_cli(loop, test_app, sanic_client):
    """
    TestClient instance for a given Sanic app.

    :return: TestClient object
    """
    return loop.run_until_complete(sanic_client(test_app))


# @pytest.fixture(autouse=True)
@pytest.fixture
def clear_database():
    """
    Clear tables in database before running a test.
    """
    ...


# @pytest.fixture(autouse=True)
@pytest.fixture
async def fill_database(loop, email, password, user_id, total_count):
    """
    Populate test database with account fixtures before running a test.
    """
    await db.set_bind(DSN)

    for i in range(1, total_count + 1):
        await User.create()

    await db.pop_bind().close()


@pytest.fixture
async def auth_tokens(user_id):
    """
    Create access and refresh token fixtures.

    :param user_id: id of test user
    :return dict with access and refresh tokens
    """
    return make_access_and_refresh_tokens(user_id)


@pytest.fixture
async def access_token(auth_tokens):
    """
    Get the access token fixture.

    :param auth_tokens: access and refresh token fixtures
    :return access token
    """
    return auth_tokens['token'].decode()


@pytest.fixture
async def refresh_token(auth_tokens):
    """
    Get the refresh token fixture.

    :param auth_tokens: access and refresh token fixtures
    :return refresh token
    """
    return auth_tokens['refresh_token']


@pytest.fixture
def auth_header(access_token):
    """
    Get an authorization HTTP header containing the access token fixture.

    :param access_token: access token fixture
    :return authorization header as dictionary
    """
    return {'Authorization': f"Bearer {access_token}"}


@pytest.fixture(scope='session')
def email():
    """

    :return: email fixture
    """
    return 'test@mail.com'


@pytest.fixture(scope='session')
def password():
    """

    :return: test user's password fixture
    """
    return 'secret'


@pytest.fixture(scope='session')
def user_id():
    """

    :return: id of the test user
    """
    return 1


@pytest.fixture(scope='session')
def total_count():
    """

    :return: number of users in test database
    """
    return 10
