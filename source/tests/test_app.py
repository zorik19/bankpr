from fwork.common.http import HTTPStatus


class TestAccounts:
    """
    Test cases related to '/accounts' routes.
    """

    async def test_register_account(self, test_cli):
        """
        Register account with email and password as authentication data.
        """
        ...
        email = 'test_register@mail.com'
        password = 'another_secret'
        role_id = 1

        profile = {
            'first_name': 'alex',
            'last_name': 'mur',
            'gender': 'male',
            'birthdate': '19-04-1988',
            'avatar': 'abc',
            }
        response = await test_cli.post(
            '/api/v1/accounts/',
            json={
                'email': email,
                'password': password,
                'role_id': role_id,
                'profile': profile
                })
        assert response.status == HTTPStatus.OK

