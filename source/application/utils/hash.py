import string
import random

from werkzeug.security import generate_password_hash, check_password_hash

from source.constants import HASH_METHOD, PASSWORD_MIN_LENGTH, SALT_LENGTH


def make_hash(value: str) -> str:
    """

    :return: generated hash
    """
    return generate_password_hash(
        password=value, method=HASH_METHOD, salt_length=SALT_LENGTH)


def check_hash(hash: str, value: str) -> bool:
    """

    :return: bool indicating whether the value matches the hash
    """
    return check_password_hash(
        pwhash=hash, password=value)


def generate_password(length=PASSWORD_MIN_LENGTH):
    choice = string.ascii_letters + string.digits
    return ''.join(random.sample(choice, length))