from datetime import datetime, timedelta

import jwt
from jwt import DecodeError, ExpiredSignatureError

from fwork.common.auth.constants import AUTH_TOKEN_ALGORITHM
from fwork.common.auth.token import get_token_payload
from fwork.common.http import HTTPStatus
from source.logger import get_logger
from source.settings import AUTH_TOKEN_EXP_TIME, AUTH_TOKEN_SECRET, REFRESH_TOKEN_EXP_TIME, REFRESH_TOKEN_SECRET


def make_access_and_refresh_tokens(user_id: int,
                                   pass_change_time: float,
                                   role_id: int = None,
                                   role_name: str = None,
                                   anonymous: bool = False,
                                   exp: int = AUTH_TOKEN_EXP_TIME,
                                   refresh_exp: int = REFRESH_TOKEN_EXP_TIME) -> dict:
    """

    :return: dict with access and refresh tokens
    """
    if type(pass_change_time) is datetime:
        pass_change_time = pass_change_time.timestamp()

    access_token, payload = generate_token(sub=user_id,
                                           pct=pass_change_time,
                                           exp=exp,
                                           role_id=role_id,
                                           role_name=role_name,
                                           anonymous=anonymous)

    refresh_token, payload = generate_token(sub=user_id,
                                            exp=refresh_exp,
                                            key=REFRESH_TOKEN_SECRET,
                                            pct=pass_change_time,
                                            role_id=role_id,
                                            role_name=role_name,
                                            anonymous=anonymous)

    tokens = {
        'token': access_token,
        'refresh_token': refresh_token
        }

    return tokens


async def verify_token(redis_pool, token: str, key: str = AUTH_TOKEN_SECRET) -> tuple:
    """
    TODO: add time check in redis
    Verifies user's JWT tokens (access_token, refresh_token): tries to decode payload + compares pass_change_time

    return: tuple: (payload: dict, code: int, description: str)
    """
    log = get_logger('tokens')

    payload_not_verified = None

    try:
        payload_not_verified = get_token_payload(token, verify=False, key=key)
        log.debug(f'verify_token: token: {str(token)}, payload_not_verified: {payload_not_verified}')
    except:
        log.debug(f'verify_token: DecodeError or KeyError -> HTTPStatus.UNAUTHORIZED: token: {str(token)}, '
                  f'payload_not_verified: {payload_not_verified}')

    try:
        payload = get_token_payload(token, verify=True, key=key)
    except ExpiredSignatureError:
        log.debug(f'verify_token: ExpiredSignatureError -> HTTPStatus.UNAUTHORIZED: token: {str(token)}, '
                  f'payload_not_verified {payload_not_verified}')
        return {}, HTTPStatus.UNAUTHORIZED, 'Auth token has expired'
    except (DecodeError, KeyError):
        log.debug(f'verify_token: DecodeError or KeyError -> HTTPStatus.UNAUTHORIZED: token: {str(token)}, '
                  f'payload_not_verified {payload_not_verified}')
        return {}, HTTPStatus.UNAUTHORIZED, 'Auth token is not valid'

    pass_change_time_token = payload.get('pct')

    if pass_change_time_token is None and payload.get('role_name') == 'PIN':
        log.debug(f'verify_token: do not check pass_change_time for a PIN-authorized device -> HTTPStatus.OK: '
                  f'payload: {payload}')
        return payload, HTTPStatus.OK, 'Auth token is valid'

    if pass_change_time_token is None:
        log.debug(f'verify_token: pass_change_time_token is None -> HTTPStatus.UNAUTHORIZED: payload: {payload}')
        return {}, HTTPStatus.UNAUTHORIZED, 'Auth token has expired'

    log.debug(f'verify_token: pass_change_time <= pass_change_time_token -> HTTPStatus.OK: payload: {payload}')

    return payload, HTTPStatus.OK, 'Auth token is valid'


def generate_token(sub: int,
                   exp: int = AUTH_TOKEN_EXP_TIME,
                   pct: float = None,
                   key: str = AUTH_TOKEN_SECRET,
                   algorithm: str = AUTH_TOKEN_ALGORITHM,
                   role_id: int = None,
                   role_name: str = None,
                   anonymous: bool = False,
                   ) -> (str, dict):
    """
    Encode JWT token.

    :param sub: subject claim тут живет айдишник пользователя
    :param exp: expiration time claim
    :param pct: user's last change pass time claim
    :param key: JWT encoding key
    :param algorithm: JWT encoding algorithm
    :param role_id: User auth provider ID
    :param role_name: User auth provider name (e.g.: facebook, google)
    :param anonymous: If the User anonymous (default: False , e.g.: User has only device no email)
    :return: JWT token
    """
    if pct is None:
        pct = datetime.utcnow().timestamp()

    iat = datetime.utcnow()

    payload = {
        'sub': sub,
        'iat': iat,
        'exp': iat + timedelta(seconds=exp),
        'pct': pct,
        'role_id': role_id,
        'role_name': role_name,
        'anonymous': anonymous
        }

    token = jwt.encode(payload=payload, key=key, algorithm=algorithm)

    return token, payload
