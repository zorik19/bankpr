from fwork.common.http import RESP_TYPE_ERRORS
from source.application.response import Response, ResponseContainer
from source.constants import  EXCEPTION_TO_HTTP_STATUS
from fwork.common.http import HTTPStatus


class AuthProviderException(Exception):
    pass


def process_exception(exception: Exception) -> Response:
    """
    Pass exception name and message to HTTP Response object.

    :return: HTTP Response with exception name and message serialized in JSON API format
    """
    exc_name = exception.__class__.__name__
    exc_msg = str(exception)

    return Response(
        response_container=ResponseContainer(
            response={exc_name: exc_msg},
            type=RESP_TYPE_ERRORS,
        ),
        status=EXCEPTION_TO_HTTP_STATUS.get(exc_name, HTTPStatus.INTERNAL_ERROR)
    )
