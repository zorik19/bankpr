from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from aiosmtplib import SMTP

from fwork.common.email.settings import MAIL_SENDER
from source.logger import get_logger

logger = get_logger('email')


async def send_registration_email(smtp: SMTP, email: str, body: str):
    """
    Send email with login password info to employee
    :param smtp - smtp client
    :param email - email where to send credentials
    :param password - newly generated password
    """

    msg = MIMEMultipart('alternative')
    msg['From'] = MAIL_SENDER
    msg['To'] = email
    msg['Subject'] = 'Confirmation Email'

    body = MIMEText(body, 'html')
    msg.attach(body)

    resp = await smtp.send_message(msg)
    logger.debug(f'Email sent to address {email} with response {resp}')
