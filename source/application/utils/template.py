import os
import sys

from source.logger import get_logger

log = get_logger('template')


# TODO: if many templates - use jinja2
def load_templates(tmpl_dir: str, body_tmpl: str) -> str:
    """
    Loads body into in-memory html template
    :param tmpl_dir: directory where html templates are stored
    :param body_tmpl: name of file, where html template stored
    :return:in-memory html template of player
    """
    log = get_logger('template')
    body_path = os.path.join(tmpl_dir, body_tmpl)
    if os.path.exists(body_path):
        with open(body_path, 'r') as f_body:
            body = f_body.read()
        return body
    else:
        log.error("Template file doesn't exist")
        sys.exit(1)
