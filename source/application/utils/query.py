from sqlalchemy.sql import Select

from fwork.common.db.postgres.conn_async import db
from source.models.company import Company
from source.models.department import Department
from source.models.user import User


def company_by_user(user_id: int) -> Select:
    """
    Get sa.Select query (based ob Gino db) of companies linked to user.

    :param user_id:
    :return:
    """
    query = db.select([*Company.t.c]) \
        .select_from(User.t \
                     .join(Department.t) \
                     .join(Company.t)) \
        .where(User.id == user_id) \
        .group_by(User.id) \
        .group_by(Company.id)

    return query
