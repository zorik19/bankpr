from datetime import datetime

from sanic.response import HTTPResponse
from sanic.views import HTTPMethodView
from sanic_openapi import doc

from fwork.common.http.response import (BadRequestResponse, SingleEntityResponse, UnauthorizedResponse)
from fwork.common.logs.sanic_helpers import TrackedRequest
from fwork.common.openapi.spec import DocMixin, error_responses
from fwork.common.openapi.spec import request_body, single_response
from source.application.utils.hash import check_hash
from source.application.utils.token import make_access_and_refresh_tokens
from source.logger import get_logger
from source.models.role import Role
from source.models.user import User
from source.schemas.account import CredentialsSchema
from source.schemas.response_schemas.token import TokenSchema


class AuthView(DocMixin, HTTPMethodView):

    @doc.summary('Log in a user')
    @doc.description('Log in a user by an email/phone + password pair if registered.')
    @doc.consumes(request_body(CredentialsSchema), location='body')
    @doc.response(200, single_response(TokenSchema), description='Access and refresh token')
    @error_responses(
        {400: 'Request data invalid / User doesn\'t exist or was inactivated',

         401: 'Incorrect password.'}, )
    async def post(self, request: TrackedRequest) -> HTTPResponse:
        """
        Login user if registered.

        :return HTTP Response with newly generated access and refresh token.
        """
        log = get_logger('auth')

        credentials = CredentialsSchema().load(request.json)

        password = credentials.pop('password')
        email = credentials.get('email')
        phone_number = credentials.get('phone_number')

        user = None

        if email:
            user = await User.query \
                .where(User.email == email) \
                .where(User.is_banned == False) \
                .where(User.is_deleted == False).gino.first()

        elif phone_number:
            user = await User.query \
                .where(User.phone_number == phone_number) \
                .where(User.is_banned == False) \
                .where(User.is_deleted == False).gino.first()

        if not user:
            err = f"user doesn't exist or was inactivated"
            log.warning(err)
            return BadRequestResponse(err)

        if not check_hash(
                hash=user.password,
                value=password):
            msg = 'Incorrect password.'
            log.warning(msg)
            return UnauthorizedResponse(msg)

        role = await Role.get(user.role_id)

        tokens = make_access_and_refresh_tokens(
            user_id=user.id,
            pass_change_time=datetime.now().timestamp(),
            role_id=role.id,
            role_name=role.name
            )

        log.debug('tokens created')
        return SingleEntityResponse(tokens, TokenSchema)
