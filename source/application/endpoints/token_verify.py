from sanic.request import Request
from sanic.views import CompositionView

from fwork.common.http import HTTPStatus
from source.application.response import Response

from source.application.utils.token import get_auth_token_from_request


async def verify_token_handler(request: Request) -> Response:
    """
    Check if JWT token exists in Redis.

    :return: HTTP Response with status code.
    """
    token = get_auth_token_from_request(request)
    if not token:
        return Response(status=HTTPStatus.UNAUTHORIZED)

    with await request.app.redis as redis_provider:
        token_exists = await redis_provider.exists(token)

    if not token_exists:
        return Response(status=HTTPStatus.UNAUTHORIZED)

    return Response(status=HTTPStatus.OK)


token_verify_view = CompositionView()
token_verify_view.add(['GET'], verify_token_handler)
