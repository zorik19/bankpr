import asyncio

from sanic.response import HTTPResponse
from sanic.views import HTTPMethodView
from sanic_openapi import doc

from fwork.common.auth import authorized
from fwork.common.auth.token import get_auth_payload_from_request
from fwork.common.db.postgres.conn_async import db
from fwork.common.http.response import BadRequestResponse, CreatedResponse, NotFoundResponse, PagedEntitiesResponse, \
    SingleEntityResponse, UnauthorizedResponse
from fwork.common.logs.sanic_helpers import TrackedRequest
from fwork.common.openapi.spec import DocMixin, error_responses, many_response, request_body, single_response
from source.application.utils.email import send_registration_email
from source.application.utils.hash import generate_password, make_hash
from source.application.utils.query import company_by_user
from source.constants import SUPERVISOR_DEPARTMENT_NAME
from source.logger import get_logger
from source.models.company import Company
from source.models.department import Department
from source.models.user import User
from source.schemas.company import CompanyInitSchema, CompanySchema
from source.schemas.response_schemas.account import AccountResponseGetSchema
from source.schemas.response_schemas.company import CompanyResponseSchema

log = get_logger('company')


class CompanyView(DocMixin, HTTPMethodView):

    @doc.summary('Create new company')
    @doc.description('Create new company for authorized user')
    @doc.consumes(request_body(CompanySchema), location='body')
    @doc.response(200, single_response(CompanyResponseSchema), description='Company')
    @error_responses(400, 401)
    @authorized
    async def post(self, request: TrackedRequest) -> HTTPResponse:
        data = CompanySchema().load(request.json)
        payload = get_auth_payload_from_request(request)
        user_id = payload['sub']

        user = await User.query.where(User.id == user_id).gino.first()

        if not user.id:
            # fool detector - if you have token it means you have ID in db
            return NotFoundResponse('No such user!')

        company = await company_by_user(user_id).gino.first()

        if company and company.id:  # join can return as None as epmpty rowproxy
            return BadRequestResponse(f'User {user_id} already has an registered company')

        else:
            company = await Company.create(**data)

            if not user.department_id:
                log.info('Create supervisor department for user')
                # TODO: shoud be in a transaction
                department = await Department.create(name=SUPERVISOR_DEPARTMENT_NAME, company_id=company.id)
                await user.update(department_id=department.id).apply()

        return SingleEntityResponse(company, CompanyResponseSchema)

    @doc.summary('Get company for registered user')
    @doc.description('Get company info for registered user')
    @doc.response(200, many_response(CompanyResponseSchema), description='Company info')
    @error_responses(400, 401, {404: 'There is no any companies for user yet!'})
    @authorized
    async def get(self, request: TrackedRequest) -> HTTPResponse:
        payload = get_auth_payload_from_request(request)
        user_id = payload['sub']

        companies = await company_by_user(user_id).gino.all()

        if not companies:
            return NotFoundResponse('There is no any companies for user yet!')

        return PagedEntitiesResponse(companies, AccountResponseGetSchema)


class CompanyInitView(DocMixin, HTTPMethodView):
    @doc.summary('Initialize new company creation')
    @doc.description('Create new company with departments and accounts for registered user')
    @doc.consumes(request_body(CompanyInitSchema), location='body')
    @doc.response(201, 'Created', description='Company created successfully')
    @error_responses(400, 401)
    @authorized
    async def post(self, request: TrackedRequest, user_id: int) -> HTTPResponse:
        payload = get_auth_payload_from_request(request)
        user_id_from_token = payload['sub']
        # TODO: check permissions rights of user - is it allowed to create companies to him

        if user_id_from_token != user_id:
            return UnauthorizedResponse('user ID from request and token are not the same')

        data = CompanyInitSchema().load(request.json)

        # check already existed companies
        company = await company_by_user(user_id).gino.first()
        if company and company.id:  # join can return as None as empty rowproxy
            return BadRequestResponse(f'User {user_id} already has an registered company')

        user = await User.get(user_id)
        error = None
        async with db.transaction() as tx:
            company = await Company.create(name=data['name'],
                                           business_type_id=data['business_type_id'],
                                           organization_id=data['organization_id'],
                                           site=data.get('site'),
                                           address=data.get('address'))

            if not user.department_id:
                log.info('Create supervisor department for user')
                department = await Department.create(name=SUPERVISOR_DEPARTMENT_NAME, company_id=company.id)
                await user.update(department_id=department.id).apply()
            send_email_coros = []
            for department_data in data['departments']:
                department = await Department.create(name=department_data['name'],
                                                     company_id=company.id,
                                                     description=department_data.get('description'))

                for user_data in department_data['accounts']:
                    employee_email = user_data['email']
                    employee = await User.query.where(User.email == employee_email).gino.first()
                    if employee and employee.id:
                        # TODO: if employee already has account as freelancer? need flag `is_freelance`
                        msg = f'User with email {employee_email} already registered'
                        log.info(msg)
                        error = msg
                        tx.raise_rollback()  # explicit rollback

                    employee_password = generate_password()
                    await User.create(department_id=department.id, password=make_hash(employee_password), **user_data)

                    msg_body = request.app.employee_template.format(email=employee_email, password=employee_password)

                    send_email_coros.append(send_registration_email(smtp=request.app.smtp,
                                                                    email=employee_email,
                                                                    body=msg_body))

        if error:
            return BadRequestResponse(error)

        log.debug('Sending emails')
        await asyncio.gather(*send_email_coros)
        log.debug('Emails are sent')
        return CreatedResponse()
