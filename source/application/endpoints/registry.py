from sanic.response import HTTPResponse
from sanic.views import HTTPMethodView
from sanic_openapi import doc

from fwork.common.http.response import PagedEntitiesResponse
from fwork.common.logs.sanic_helpers import TrackedRequest
from fwork.common.openapi.spec import DocMixin, error_responses, single_response
from source.logger import get_logger
from source.models.business_type import BusinessType
from source.models.organization_type import OrganizationType
from source.models.permission import Permission
from source.models.role import Role
from source.schemas.response_schemas.registry import BusinessTypeResponseSchema, OrganizationResponseSchema, \
    PermissionResponseSchema, \
    RoleResponseSchema

log = get_logger('roles')


class RolesView(DocMixin, HTTPMethodView):

    @doc.summary('Get list of roles')
    @doc.description('Get list of available roles')
    @doc.response(200, single_response(RoleResponseSchema), description='List of permissions')
    @error_responses(401)
    async def get(self, request: TrackedRequest) -> HTTPResponse:
        roles = await Role.query.gino.all()

        return PagedEntitiesResponse(roles, RoleResponseSchema)


class PermissionsView(DocMixin, HTTPMethodView):

    @doc.summary('Get list of permissions')
    @doc.description('Get list of available permissions')
    @doc.response(200, single_response(PermissionResponseSchema), description='List of permissions')
    @error_responses(401)
    async def get(self, request: TrackedRequest) -> HTTPResponse:
        permissioins = await Permission.query.gino.all()

        return PagedEntitiesResponse(permissioins, RoleResponseSchema)


class BusinessTypeView(DocMixin, HTTPMethodView):

    @doc.summary('Get list of business types')
    @doc.description('Get list of available business types')
    @doc.response(200, single_response(BusinessTypeResponseSchema), description='List of business types')
    @error_responses(401)
    async def get(self, request: TrackedRequest) -> HTTPResponse:
        business_types = await BusinessType.query.gino.all()

        return PagedEntitiesResponse(business_types, RoleResponseSchema)


class OrganizationTypeView(DocMixin, HTTPMethodView):

    @doc.summary('Get list of organization types')
    @doc.description('Get list of available organization types')
    @doc.response(200, single_response(OrganizationResponseSchema), description='List of organization types')
    @error_responses(401)
    async def get(self, request: TrackedRequest) -> HTTPResponse:
        business_types = await OrganizationType.query.gino.all()

        return PagedEntitiesResponse(business_types, RoleResponseSchema)
