from datetime import datetime

from sanic.response import HTTPResponse
from sanic.views import HTTPMethodView
from sanic_openapi import doc

from fwork.common.auth import authorized
from fwork.common.auth.token import get_auth_payload_from_request
from fwork.common.http.response import BadRequestResponse, SingleEntityResponse
from fwork.common.logs.sanic_helpers import TrackedRequest
from fwork.common.openapi.spec import DocMixin, error_responses, request_body, single_response
from fwork.common.sanic.crud.requests import raw_args
from source.application.utils.hash import make_hash
from source.application.utils.query import company_by_user
from source.application.utils.token import make_access_and_refresh_tokens
from source.logger import get_logger
from source.models.role import Role
from source.models.user import User
from source.schemas.account import AccountFilterSchema, AccountRegisterSchema
from source.schemas.response_schemas.account import AccountResponseGetSchema
from source.schemas.response_schemas.token import TokenSchema

log = get_logger('accounts')


class Accounts(DocMixin, HTTPMethodView):

    @doc.summary('Create new account')
    @doc.description('Create new account and get access and refresh token as response')
    @doc.consumes(request_body(AccountRegisterSchema), location='body')
    @doc.response(200, single_response(TokenSchema), description='Access and refresh token')
    @error_responses(400, {404: 'User doesn\'t exist or was inactivated'})
    async def post(self, request: TrackedRequest) -> HTTPResponse:
        data = AccountRegisterSchema().load(request.json)
        password = data.pop('password')
        hashed_pass = make_hash(password)

        role = await Role.get(data['role_id'])
        user = await User.query.where(User.email == data['email']).gino.first()

        if user:
            err = f"user with email {data['email']} already exist, try to login instead"
            log.warning(err)
            return BadRequestResponse(err)

        user = await User.create(password=hashed_pass, **data)
        tokens = make_access_and_refresh_tokens(
            user_id=user.id,
            pass_change_time=datetime.now().timestamp(),
            role_id=role.id,
            role_name=role.name
            )
        return SingleEntityResponse(tokens, TokenSchema)

    @doc.summary('Get account info')
    @doc.description('Get account info')
    @doc.consumes(doc.Boolean(name='company', required=False, description='Get account with company info?'),
                  location='query')  # todo: rm this param in future
    @doc.response(200, single_response(AccountResponseGetSchema), description='Account info')
    @error_responses(400, 401, {404: 'User doesn\'t exist or was inactivated'})
    @authorized
    async def get(self, request: TrackedRequest) -> HTTPResponse:
        raw_params = raw_args(request)
        params = AccountFilterSchema().load(raw_params)
        with_company = params.get('company')

        payload = get_auth_payload_from_request(request)
        user_id = payload['sub']
        account = await User.query.where(User.id == user_id).gino.first()
        account = account.to_dict()
        account.pop('password')

        if with_company:
            company = await company_by_user(user_id).gino.all()

            account['company'] = company

        return SingleEntityResponse(account, AccountResponseGetSchema)
