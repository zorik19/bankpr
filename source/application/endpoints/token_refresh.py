from datetime import datetime
from http.client import HTTPResponse

from jwt import ExpiredSignatureError, InvalidSignatureError
from sanic.views import HTTPMethodView
from sanic_openapi import doc

from fwork.common.auth.token import get_token_payload
from fwork.common.http.response import BadRequestResponse, SingleEntityResponse, UnauthorizedResponse
from fwork.common.logs.sanic_helpers import TrackedRequest
from fwork.common.openapi.spec import DocMixin, error_responses, request_body, single_response
from source.application.utils.token import \
    make_access_and_refresh_tokens
from source.schemas.account import TokenRefreshSchema
from source.schemas.response_schemas.token import TokenSchema
from source.settings import REFRESH_TOKEN_SECRET


class RefreshTokenView(DocMixin, HTTPMethodView):

    @doc.summary('Refresh token')
    @doc.description('Check token and take a new pair of access and refresh tokens if succeed.')
    @doc.consumes(request_body(TokenRefreshSchema), location='body')
    @doc.response(200, single_response(TokenSchema), description='Access and refresh token')
    @error_responses(400, 401)
    async def post(self, request: TrackedRequest) -> HTTPResponse:

        global payload
        token = TokenRefreshSchema().load(request.json)

        # Almost improbable case when a token has expired after check in the decorator
        try:
            payload = get_token_payload(token['refresh_token'], key=REFRESH_TOKEN_SECRET,  verify=True)
        except ExpiredSignatureError:
            return UnauthorizedResponse('Authorization token is not valid')
        except InvalidSignatureError:
            return BadRequestResponse(f'Invalid signature for token')

        tokens = make_access_and_refresh_tokens(
            user_id=payload['sub'],
            pass_change_time=datetime.now().timestamp(),
            role_id=payload['role_id'],
            role_name=payload['role_name']
            )

        return SingleEntityResponse(tokens, TokenSchema)
